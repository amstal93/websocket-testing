# websocket-testing

A quick setup with a Flask-socketio webserver, a message producer, and a rabbitmq server.

Originally I tried to using Pika (see the tag `kinda_works`). This wasn't super elegant and required me to manage threads.

This incarnation originally attempted to instantiate a Kombu manager and pass it to the SocketIO constructer, but it turns out it's pretty simple to just base an AMQP URI and let the module figure it out. 

We do monkey patching but we still end up with one weird thread error - i'm not sure what that's about and I've exhausted myself trying to figure out how to deal with it.

NOTES: Don't use librabbitmq - it's C and can't be monkey patched, use `pip install amqp`.˚